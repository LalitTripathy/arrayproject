function find(elements, cb) {

    if (!Array.isArray(elements)){
        return ('Invalid input, please input array');
    }

    let flag=0;
    for (var index = 0; index < elements.length; index++) {
        if (cb(elements[index]) ) {
            flag = 1;

            return elements[index];
        }
    }
    if (flag==0){
        return 'undefined';
    }

    return elements.myfind(cb)
}

module.exports={find};