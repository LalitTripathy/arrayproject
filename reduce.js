function reduce(elements, cb, startingValue) {
    var accumulator; // This accumulator is used to store value passed to cb(function).
    
    if ( !Array.isArray(elements) ){
      cb('Invalid input, please input array');
    }
  
    if (startingValue === undefined){
      accumulator = elements[0];
    }else {
      accumulator = startingValue;
    }
  
    for (var index = 0; index < elements.length; index++) {
      accumulator = cb(accumulator, elements[index])
      
      }
    return accumulator;
  }
  module.exports = {reduce};