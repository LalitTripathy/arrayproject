function filter(elements, cb) {
    
        if (!Array.isArray(elements)){
            return ('Invalid input, please input array');
        }
    
        let array=[];
        for (var index = 0; index < elements.length; index++) {
            if (cb(elements[index])) {
                array.push(elements[index]);
            }
        }
        return array;
    }
    
    module.exports={filter};