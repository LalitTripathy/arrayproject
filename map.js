function myMap(elements, cb) {

    let array=[];

    // if(!elements || !Array.isArray(elements)) return [];
    

    for (let index = 0; index < elements.length; index++){
        const result = cb(elements[index],index);
        array.push(result);
    }
    return array;
}
module.exports={myMap};