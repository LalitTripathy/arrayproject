function flatten(elements) {

    let array = [];

    if (!Array.isArray(elements)){
        return ('Invalid input, please input array');
    }

    for (let index = 0; index < elements.length; index++){
        if(Array.isArray(elements[index])){
            array = array.concat(flatten(elements[index]));
        }else{
            array.push(elements[index]);
        }
    }
    return array;
}

module.exports={flatten}