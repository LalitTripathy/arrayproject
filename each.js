function myEach(elements, cb) {

    let flag=0

    if (!Array.isArray(elements)){
        cb('Invalid input, please input array');
        flag=1;
    }
    
    if (flag==0){
        
        for (let index = 0; index < elements.length; index++) {
            cb(elements[index], index);
        }
    }
}

module.exports = myEach;